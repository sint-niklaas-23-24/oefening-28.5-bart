﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._5
{
    internal class Printer
    {
        private bool _busy = false;
        private string _naam;

        public Printer() { }
        public Printer(string naam)
        {
            Naam = naam;
        }

        public bool Busy { get { return _busy; } set {  _busy = value; } }
        public string Naam { get { return _naam; } set { _naam = value; } }

        public void Reset()
        {
            Busy = false;
        }
        public override string ToString()
        {
            string status;

            if (Busy == false)
            {
                status = Naam + ": wachtende op een print opdracht.";
            }
            else
            {
                status = Naam + ": bezig met een print opdracht.";
            }

            return status;
        }
    }
}
