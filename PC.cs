﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._5
{
    internal class PC
    {
        private List<Printer> _printers;

        public PC() 
        {
            Printers = new List<Printer>();
        }

        public List<Printer> Printers { get { return _printers; } set { _printers = value; } }

        public void AddPrinter(Printer p) 
        {
            Printers.Add(p);
        }

        //public bool PrintAf()
        //{
        //    return true;
        //}
    }
}
