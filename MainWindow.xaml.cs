﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace Oefening_28._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        PC dezePC = new PC();
        Printer printer1 = new Printer("printer 1");
        Printer printer2 = new Printer("printer 2");
        Printer printer3 = new Printer("printer 3");
        Printer printer4 = new Printer("printer 4");


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dezePC.AddPrinter(printer1);
            dezePC.AddPrinter(printer2);
            dezePC.AddPrinter(printer3);
            dezePC.AddPrinter(printer4);

            imgPrinter1.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
            imgPrinter2.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
            imgPrinter3.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
            imgPrinter4.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));

            lblPrinter1.Content = printer1.ToString();
            lblPrinter2.Content = printer2.ToString();
            lblPrinter3.Content = printer3.ToString();
            lblPrinter4.Content = printer4.ToString();
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            bool printerBezig = false;
            int printerNummer = 0;
            try
            {
                foreach (Printer p in dezePC.Printers)
                {
                    if (p.Busy == false)
                    {
                        p.Busy = true;
                        printerBezig = true;
                        RefreshLabels();
                        break;
                    }

                    printerNummer++;
                }

                if (!printerBezig)
                {
                    MessageBox.Show("Er is geen printer beschikbaar.");
                }
                else
                {
                    PrinterAfbeeldingWijzigen(printerNummer);
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het printen.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnResetPrinter1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (printer1.Busy == true)
                {
                    printer1.Busy = false;
                    imgPrinter1.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
                    lblPrinter1.Content = printer1.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het resetten van de printer.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnResetPrinter2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (printer2.Busy == true)
                {
                    printer2.Busy = false;
                    imgPrinter2.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
                    lblPrinter2.Content = printer2.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het resetten van de printer.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnResetPrinter3_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (printer3.Busy == true)
                {
                    printer3.Busy = false;
                    imgPrinter3.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
                    lblPrinter3.Content = printer3.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het resetten van de printer.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void btnResetPrinter4_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (printer4.Busy == true)
                {
                    printer4.Busy = false;
                    imgPrinter4.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_notbusy.jpg", UriKind.Absolute));
                    lblPrinter4.Content = printer4.ToString();
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het resetten van de printer.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        public void RefreshLabels()
        {
            lblPrinter1.Content = printer1.ToString();
            lblPrinter2.Content = printer2.ToString();
            lblPrinter3.Content = printer3.ToString();
            lblPrinter4.Content = printer4.ToString();
        }
        private void PrinterAfbeeldingWijzigen(int printerNummer)
        {
            try
            {
                switch (printerNummer)
                {
                    case 0:
                        imgPrinter1.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_busy.gif", UriKind.Absolute));
                        break;
                    case 1:
                        imgPrinter2.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_busy.gif", UriKind.Absolute));
                        break;
                    case 2:
                        imgPrinter3.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_busy.gif", UriKind.Absolute));
                        break;
                    case 3:
                        imgPrinter4.Source = new BitmapImage(new Uri(Environment.CurrentDirectory + @"\images\printer_busy.gif", UriKind.Absolute));
                        break;
                    default:
                        MessageBox.Show("Deze printer heeft geen afbeelding.");
                        break;
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgetreden bij het refreshen van de afbeelding.", "Fout", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
    }
}